<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<?php  get_template_part( 'components/world-map'); ?>
<section id="to-top" class="front-page-main">
	<div class="container">
		
		<div class="row">
			<div class="col-md-8">
				<div class="blog-grid-fp">
					<?php
					$args = array( 'posts_per_page' => 8,
					'post_type' => 'post',
					'orderby'   => 'date',
					'order'    => 'DESC'
					);
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					
					
					<?php get_template_part( 'components/city-blog-info-front-page'); ?>
					
					
					
					
					<?php endforeach;
					wp_reset_postdata(); ?>
				</div>
			</div>
			
			<div class="col-md-4">
				<!-- <div class="hideme"> -->
					
					
					<div class="main-image-us-fp" id="myAnchor">
						<?php if( get_field('main_image') ): ?>
						<a href="<?php echo get_permalink(get_page_by_title('About Us')); ?>"><img class="img-responsive" src="<?php the_field('main_image'); ?>" /></a>
						<?php endif; ?>
						
					</div>
					<div class="content-us-fp">
						<h2 class="fancy"><span><?php the_field('sub_heading'); ?></span></h2>
						<div class="text-content">
							<p><?php the_field('introduction'); ?></p>
						</div>
						
					</div>
				<!-- </div> -->
				<div class="stats-section">
					<div class="hideme countries-travelled spacing">
						<i class="fa fa-globe-africa"></i>
						<h3> Countries Travelled: <br/> <span><?php the_field('countries_travelled') ?></span></h3>
					</div>
					<hr>
					<hr>
					<div class="hideme next-trip spacing">
						<i class="fa fa-plane"></i>
						<h3>Next Holiday: <br/><span><?php the_field('next_trip') ?></span></h3>
					</div>
					<hr>
					<hr>
					<div class="hideme dream-trip spacing">
						<i class="fa fa-umbrella-beach"></i>
						<h3>Dream destination: <br/> <span><?php the_field('dream_destination') ?></span></h3>
					</div>
					<hr>
					<hr>
					<div class="hideme where-moldova spacing">
						<i class="fa fa-map-signs"></i>
						<h3>Where's Moldova?</h3>
						<p class="where-moldova-text"><?php the_field('wheres_moldova_text') ?></p>
					</div>
					<hr>
					<hr>
					<div class="hideme follow-us">
						<h3>Connect with us</h3>
						<div class="follow-us-row-one">
							<a class="fab fa-youtube" target="_blank" href="https://www.youtube.com/channel/UCw_vNR2ovhhOVMdhSxE8t3A"></a>
							<a class="fab fa-pinterest-square" target="_blank" href="https://www.pinterest.co.uk/mapscratchers/"></a>
							<a class="fab fa-facebook-square" target="_blank" href="https://www.facebook.com/mapscratchers/"></a>
							
						</div>
						<div class="follow-us-row-two"> 
							<a class="fa fa-envelope" target="_blank" href="mailto:mapscratchersblog@gmail.com?Subject=Hello%20Map%20Scratchers"></a>
							<a class="fab fa-instagram" target="_blank" href="https://www.instagram.com/mapscratchers/"></a>
							<a class="fa fa-heart" target="_blank" href="https://www.bloglovin.com/@anispee"></a>
							
						</div>
					</div>
				</div>
				
			</div>
		</div>

	</div>
	
</section>
<section class="front-page-view-all">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<hr>
				<hr>
				<div class="view-all-blogs">
					<a class="animated-button all-blogs" href="/blogs/">View all blogs</a>
				</div>
			</div>
		</div>
	</div>
	
</section>
<?php get_template_part('components/back-to-top') ?>
<?php endwhile; ?>
<?php get_footer(); ?>