
<section class="instagram">
<div class="insta-follow">
	<a class="animated-button insta-button" href="https://www.instagram.com/mapscratchers/" target="_blank">Follow us on Instagram</a>
</div>
</section>
<?php  get_template_part( 'components/instagram'); ?>
<section class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main-footer-menu">
					<?php wp_nav_menu( array(
					'menu' => 'Footer Menu'
					) ); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="copywrite">
					&copy;<?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?> | Design &amp; Build by Sam &amp; Ani.</a>
				</div>
			
		</div>
	</div>
</div>
</section>
<?php get_template_part( 'foot' ); ?>