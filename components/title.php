<section class="top-section " style="background:linear-gradient(0deg,rgba(0,0,0,0.4),rgba(0,0,0,0.4)),url('<?php the_field('main_image'); ?>'); background-position:center; background-size:cover; background-repeat: no-repeat; background-attachment: fixed;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="main-heading">
					<h1><?php the_title(); ?></h1>
				</div>
				
			</div>
		</div>
	</div>
</section>