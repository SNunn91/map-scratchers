<?php /* $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'square' ); */?>
<div class="city-blog-image-fp">
	<div class="blog-info-wrap-grid-fp">
		<a class="blog-image-single-fp" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'blog-grid', array( 'class' => 'img-responsive' ) ); ?>
			
		</a>
		<div class="city-blog-content-fp">
			<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<hr>
			<hr>
			
			<div class="text">
				<?php $content = get_field('content'); ?>
				<p><?php echo wp_trim_words($content, 25, ' ...' ) ; ?></p>
			</div>
			<div class="city-blog-extras-fp">
				<p><?php echo get_the_date(); ?></p>
				
			</div>
			
		</div>
	</div>
</div>
<script>
jQuery(document).ready(function($) {
$gridTwo = $('.blog-grid-fp').isotope({
itemSelector: '.blog-info-wrap-grid-fp',
layoutMode: 'packery',
packery: {
gutter: 20
}
});
//Allows images to load before isotope grid loads
$gridTwo.imagesLoaded().progress( function() {
$gridTwo.isotope('layout');
});
});
</script>