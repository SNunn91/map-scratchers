<section id="mapSection" class="map-section">
  <div id="mapdiv" ></div>
  <?php if(is_page('Home')) : ?>
  <div class="scroll-down bounce">
    <a href="#myAnchor" class="scroll-chev fas fa-chevron-down "></a>
  </div>
  <?php endif; ?>
</section>
<script>
jQuery(document).ready(function($) {
//smooth scroll
$(document).on('click', '.scroll-chev[href^="#"]', function(event){
event.preventDefault();
$('html, body').animate({
scrollTop: $( $.attr(this, 'href') ).offset().top
}, 900);
});
});
</script>
<script>
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "mapdiv", {
"type": "map",
"theme": "light",
"imagesSettings": {
// "rollOverColor": "#089282",
// "rollOverScale": 3,
// "selectedScale": 3,
"selectedColor": "#089282",
"color": "#13564e"
},
"zoomControl": {
"buttonFillColor": "#fff",
"buttonIconColor": "#e4af9d"
},
"areasSettings": {
  "color" : "#d6d6d6",
  "selectedColor" : "#e28566",
  "rollOverColor" : undefined,
"autoZoom": false,
"balloonText": ""

},
"legend": {
"backgroundColor": "#fff",
"backgroundAlpha": 0.1,
"align": "center",
"data": [{
"title": "Visited",
"color": "#e4af9d"
}, {
"title": "Blogged",
"color": "#e28566"
}]
},
"dataProvider": {
"map": "worldHigh",
"getAreasFromMap": true,
    "areas" :[{
        "id": "DE",
"selectedColor" : "#e4af9d",
        "showAsSelected": true,
        "myUrl": "germany/",
        "selectable" : false,
"balloonText": "Germany"
      },
/*-----------------------
------- Blogged --------
-----------------------*/
//Italy
{
"id": "IT",
"showAsSelected": true,
"myUrl": "italy/",
"selectable" : true,
"balloonText": "Italy"
},
//Portugal
{
  "id": "PT",
  "showAsSelected": true,
  "myUrl": "portugal/",
  "selectable" : true,
"balloonText": "Portugal"
      },
//Russia
{
"id": "RU",
"showAsSelected": true,
"myUrl": "russia/",
"selectable" : true,
"balloonText": "Russia"
},
//USA
{
"id": "US",
"showAsSelected": true,
"myUrl": "usa/",
"selectable" : true,
"balloonText": "USA"
},
//China
{
"id": "CN",
"showAsSelected": true,
"myUrl": "china/",
"selectable" : true,
"balloonText":"China"
},
//Canada
{
"id": "CA",
"showAsSelected": true,
"myUrl": "canada/",
"selectable" : true,
"balloonText":"Canada"
},
//Croatia
{
"id": "HR",
"showAsSelected": true,
"myUrl": "croatia/",
"selectable" : true,
"balloonText":"Croatia"
},

/*-----------------------
------- Visited --------
-----------------------*/
//Malta
{
"id": "MT",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "malta/",
"selectable" : false,
"balloonText":"Malta"
},
//Czech Republic
{
"id": "CZ",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "czech-republic/",
"selectable" : false,
"balloonText":"Czech Republic"
},
//Spain
{
"id": "ES",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "spain/",
"selectable" : false,
"balloonText":"Spain"
},
//Iceland
{
"id": "IS",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "iceland/",
"selectable" : false,
"balloonText":"Iceland"
},
//Sweden
{
"id": "SE",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "sweden/",
"selectable" : false,
"balloonText":"Sweden"
},
// United Kingdom
{
"id": "GB",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "united-kingdom/",
"selectable" : false,
"balloonText":"United Kingdom"
},
//France
{
"id": "FR",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "france/",
"selectable" : false,
"balloonText":"France"
},
//Belgium
{
"id": "BE",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "belgium/",
"selectable" : false,
"balloonText":"Belgium"
},
//Netherlands
{
"id": "NL",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "netherlands/",
"selectable" : false,
"balloonText":"Netherlands"
},
//Poland
{
"id": "PL",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "poland/",
"selectable" : false,
"balloonText":"Poland"
},
//Switzerland
{
"id": "CH",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "switzerland/",
"selectable" : false,
"balloonText":"Switzerland"
},
//Austria
{
"id": "AT",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "austria/",
"selectable" : false,
"balloonText":"Austria"
},
//Romania
{
"id": "RO",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "romania/",
"selectable" : false,
"balloonText":"Romania"
},
//Ukraine
{
"id": "UA",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "ukraine/",
"selectable" : false,
"balloonText":"Ukraine"
},
//Moldova
{
"id": "MD",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "moldova/",
"selectable" : false,
"balloonText":"Moldova"
},
//Greece
{
"id": "GR",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "greece/",
"selectable" : false,
"balloonText":"Greece"
},
//Turkey
{
"id": "TR",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "turkey/",
"selectable" : false,
"balloonText":"Turkey"
},
//Vietnam
{
"id": "VN",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "vietnam/",
"selectable" : false,
"balloonText":"Vietnam"
},
//Luxembourg
{
"id": "LU",
"selectedColor" : "#e4af9d",
"showAsSelected": true,
"myUrl": "luxembourg/",
"selectable" : false,
"balloonText":"Luxembourg"
},
      ]
    },

"listeners": [ {
"event": "clickMapObject",
"method": function( event ) {
// check if the map is already at traget zoomLevel and go to url if it is

window.location.href = event.mapObject.myUrl;

currentObject = event.mapObject;
}
}]
} );
function clickObject( id ) {
map.clickMapObject( map.getObjectById( id ) );
}
</script>