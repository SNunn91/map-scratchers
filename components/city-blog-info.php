<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'square' );?>
<div class="blog-info-wrap" style="background: url('<?php echo $thumb['0'];?>'); background-position:center; background-size:cover; background-repeat: no-repeat; height:250px;">
	<div class="blog-info-content">
		<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
	</div>
	
	
</div>