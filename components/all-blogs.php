<section class="country-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2><a href="/blogs/">View all blogs</a></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="blog-carousel">
					<?php
					$args = array( 'posts_per_page' => -1,
					'post_type' => 'post',
					'orderby'   => 'menu_order',
					'order'    => 'DESC'
					);
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<?php get_template_part( 'components/city-blog-info'); ?>
					<?php endforeach;
					wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
jQuery(document).ready(function($) {
	$('.blog-carousel').flickity({
		// options
		cellAlign: 'left',
		groupCells:3,
		contain: false,
		pageDots: false,
		wrapAround: true,
		prevNextButtons: true
	});
	//navigation
	// $('.previous-small-team').on( 'click', function() {
	// 	$('.meet-the-team-carousel').flickity( 'previous');
	// });
	// $('.next-small-team').on( 'click', function() {
	// 	$('.meet-the-team-carousel').flickity( 'next');
	// });



});
</script>