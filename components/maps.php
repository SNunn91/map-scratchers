
<?php /* 

OLD CODE

- Pulled through country maps and marked city locations
- If statements used to define which map is used on the country page

<?php 
$country = get_the_title(); 
$destinations = get_the_title();

 ?>
<?php if($country == 'Portugal') : ?>
	<div class="map-wrap" id="portugalMap"></div>
<?php elseif ($country == 'Germany') : ?>
	<div class="map-wrap" id="germanyMap"></div>
<?php elseif ($country == 'Italy') : ?>
	<div class="map-wrap" id="italyMap"></div>
<?php elseif ($country == 'Russia') : ?>
  <div class="map-wrap" id="russiaMap"></div>
<?php elseif ($country == 'Malta') : ?>
  <div class="map-wrap" id="maltaMap"></div>
<?php elseif ($country == 'Spain') : ?>
  <div class="map-wrap" id="spainMap"></div>
<?php elseif ($country == 'Czech Republic') : ?>
  <div class="map-wrap" id="czechMap"></div>
<?php elseif ($country == 'China') : ?>
  <div class="map-wrap" id="chinaMap"></div>
<?php elseif ($country == 'USA') : ?>
  <div class="map-wrap" id="usaMap"></div>
<?php elseif ($country == 'Canada') : ?>
  <div class="map-wrap" id="canadaMap"></div>
<?php endif; ?>


<script>
//PORTUGAL MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "portugalMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
    "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
  	"color" : "#acc7dc",
	
	"rollOverColor" : "#acc7dc",
	"balloonText": ''
	// "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 14,
    "zoomLongitude": 2,
    "zoomLatitude": 40,
    "areas" :[
			{
				"id": "PT",
				"showAsSelected": true,
				"myUrl": "portugal/",
				"selectable" : false,
				"selectedColor" : "#e4af9d",

			}],
    // "linkToObject": "Lisbon",
		  
    "images": [ {

      "id": "Lisbon",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      "zoomLevel": 4,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Lisbon",
      "latitude": 	38.736946,
      "longitude": -9.142685,
      "myUrl": "#Lisbon"
    },
    {

      "id": "Sintra",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      "zoomLevel": 8,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Sintra",
      "latitude": 	38.80097,
      "longitude": -9.37826,
      "myUrl": "#Sintra"
    }


     ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>



<script>
//GERMANY MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "germanyMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 11,
    "zoomLongitude": 11,
    "zoomLatitude": 50,
     "areas" :[{
				"id": "DE",
				"showAsSelected": true,
				"myUrl": "germany/",
				"selectable" : false,
				"selectedColor": "#e4af9d",

			}],
		  
    "images": [ {

      "id": "Stuttgart",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      "zoomLevel": 8,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Stuttgart",
      "latitude": 	48.7666667,
      "longitude": 9.1833333,
      "myUrl": "#Stuttgart"
    },
    {

      "id": "Berlin",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      "zoomLevel": 8,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Berlin",
      "latitude": 	52.520645,
      "longitude": 13.409779,
      "myUrl": "#Berlin"
    }


     ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//ITALY MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "italyMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
       "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 13,
    "zoomLongitude": 14,
    "zoomLatitude": 42,
    
    "areas" :[
			{
				"id": "IT",
				"showAsSelected": true,
				"myUrl": "italy/",
				"selectable" : false,
				"selectedColor": "#e4af9d",

			}],

		  
    "images": [ {

      "id": "Rome",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Rome",
      "latitude": 41.890251,
      "longitude": 12.492373,
      "myUrl": "#Rome",
    },
    {

      "id": "Venice",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Venice",
      "latitude": 	45.444958,
      "longitude": 12.328463,
      "myUrl": "#Venice"
    },
    {

      "id": "Bologna",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Bologna",
      "latitude":   44.498955,
      "longitude": 11.327591,
      "myUrl": "#Bologna"
    },
    {

      "id": "Verona",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Verona",
      "latitude":   45.4455132,
      "longitude": 10.8604955,
      "myUrl": "#Verona"
    },
    {

      "id": "Florence",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Florence",
      "latitude":   43.769562,
      "longitude": 11.255814,
      "myUrl": "#Florence"
    },
    {

      "id": "Pisa",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Pisa",
      "latitude":   43.7067293,
      "longitude": 10.3253383,
      "myUrl": "#Pisa"
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//RUSSIA MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "russiaMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
       "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 4,
    "zoomLongitude": 40,
    "zoomLatitude": 60,
    
    "areas" :[
      {
        "id": "RU",
        "showAsSelected": true,
        "myUrl": "russia/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "Moscow",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Moscow",
      "latitude": 55.751244,
      "longitude": 37.618423,
      "myUrl": "#Moscow",
    },
    {

      "id": "St Petersburg",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "St. Petersburg",
      "latitude": 59.93863,
      "longitude": 30.31413,
      "myUrl": "#StPetersburg"
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//MALTA MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "maltaMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 25,
    "zoomLongitude": 15,
    "zoomLatitude": 35,
    
    "areas" :[
      {
        "id": "MT",
        "showAsSelected": true,
        "myUrl": "malta/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "Gozo",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Gozo",
      "latitude": 36.044300,
      "longitude": 14.251222,
      "myUrl": "#Gozo",
    },
    {

      "id": "Valletta",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Valletta",
      "latitude": 35.8984802,
      "longitude": 14.5044102,
      "myUrl": "#Valletta"
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//SPAIN MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "spainMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 12,
    "zoomLongitude": 2,
    "zoomLatitude": 40,
    
    "areas" :[
      {
        "id": "ES",
        "showAsSelected": true,
        "myUrl": "spain/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "Madrid",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Madrid",
      "latitude": 40.4378698,
      "longitude": -3.8196207,
      "myUrl": "#Madrid",
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//CZECH REPUBLIC MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "czechMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 4,
    "zoomLongitude": 40,
    "zoomLatitude": 60,
    
    "areas" :[
      {
        "id": "CZ",
        "showAsSelected": true,
        "myUrl": "czech-republic/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "Prague",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Prague",
      "latitude": 50.0593325,
      "longitude": 14.1854451,
      "myUrl": "#Prague",
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//USA MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "usaMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 5,
    "zoomLongitude": -100,
    "zoomLatitude": 40,
    
    "areas" :[
      {
        "id": "US",
        "showAsSelected": true,
        "myUrl": "usa/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "San Francisco",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "San Francisco",
      "latitude": 37.7576171,
      "longitude": -122.5776844,
      "myUrl": "#San Francisco",
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//CHINA MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "chinaMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 4,
    "zoomLongitude": 95,
    "zoomLatitude": 40,
    
    "areas" :[
      {
        "id": "CN",
        "showAsSelected": true,
        "myUrl": "china/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "Chengdu",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Chengdu",
      "latitude": 30.658228,
      "longitude": 103.7953568,
      "myUrl": "#Chengdu",
    }
    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

<script>
//CANADA MAP
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "canadaMap", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    // "rollOverColor": "#089282",
    // // "rollOverScale": 3,
    // // "selectedScale": 3,
    // "selectedColor": "#089282",
        "color": "#e4af9d"
  },

   "zoomControl": {
    "buttonFillColor": "#fff",
    "buttonIconColor": "#e4af9d" 
  },

  "areasSettings": {
    "color" : "#acc7dc",
  
  "rollOverColor" : "#acc7dc",
  "balloonText": ''
  // "rollOverOutlineColor": "#6c648b",
   
    
  },

  "dataProvider": {
    "map": "worldHigh",
    "getAreasFromMap": true,
    "centerMap" : true,
    "zoomLevel": 3,
    "zoomLongitude": -100,
    "zoomLatitude": 60,
    
    "areas" :[
      {
        "id": "CA",
        "showAsSelected": true,
        "myUrl": "toronto/",
        "selectable" : false,
        "selectedColor": "#e4af9d",

      }],

      
    "images": [ {

      "id": "Toronto",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Toronto",
      "latitude": 43.6565353,
      "longitude": -79.6010328,
      "myUrl": "#Toronto",
    },
    {
      "id": "Niagara Falls",
      "color": "#6bbaa7",
      "svgPath": targetSVG,
      // "zoomLevel": 1,
      "scale": 1,
      "rollOverScale": "2",
      "title": "Niagara Falls",
      "latitude": 43.0828162,
      "longitude": -79.0741629,
      "myUrl": "#Niagara Falls",
    }

    ]
  },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>

*/ ?>
