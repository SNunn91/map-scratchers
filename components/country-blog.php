<div class="blog-carousel">
	<?php $countryTitle = get_the_title(); ?>
	<?php if($countryTitle =='Portugal'): ?>
	<?php
	$args = array( 'posts_per_page' => -1,
	'post_type' => 'post',
	'category_name' => 'portugal',
	'orderby'   => 'menu_order',
	'order'    => 'DESC'
	);
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php $category_country = wp_get_post_terms($post->ID, 'category', array("fields" => "portugal")); ?>
	<?php get_template_part( 'components/city-blog-info'); ?>
	<?php endforeach;
	wp_reset_postdata(); ?>
</div>

<div class="blog-carousel">
	<?php $countryTitle = get_the_title(); ?>
	<?php elseif($countryTitle =='Italy'): ?>
	<?php
	$args = array( 'posts_per_page' => -1,
	'post_type' => 'post',
	'category_name' => 'italy',
	'orderby'   => 'menu_order',
	'order'    => 'DESC'
	);
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php $category_country = wp_get_post_terms($post->ID, 'category', array("fields" => "italy")); ?>
	<?php get_template_part( 'components/city-blog-info'); ?>
	<?php endforeach;
	wp_reset_postdata(); ?>
</div>

<div class="blog-carousel">
	<?php $countryTitle = get_the_title(); ?>
	<?php elseif($countryTitle =='Russia'): ?>
	<?php
	$args = array( 'posts_per_page' => -1,
	'post_type' => 'post',
	'category_name' => 'russia',
	'orderby'   => 'menu_order',
	'order'    => 'DESC'
	);
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php $category_country = wp_get_post_terms($post->ID, 'category', array("fields" => "russia")); ?>
	<?php get_template_part( 'components/city-blog-info'); ?>
	<?php endforeach;
	wp_reset_postdata(); ?>
</div>
<?php endif; ?>