<?php if(is_page('Blogs')): ?>
<?php $post_tag = get_the_tags(); ?>
<div class="city-blog-image-fp">
	<div class="blog-info-wrap-grid-fp <?php foreach($post_tag as $tag) { echo $tag->slug . '  '; }; ?>">
		<a class="blog-image-single-fp" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'blog-grid', array( 'class' => 'img-responsive' ) ); ?></a>
		<div class="city-blog-content-fp">
			<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<hr>
			<hr>
			
			<div class="text">
				<?php $content = get_field('content'); ?>
				<p><?php echo wp_trim_words($content, 25, ' ...' ) ; ?></p>
			</div>
			<div class="city-blog-extras-fp">
				<p><?php echo get_the_date(); ?></p>
				
			</div>
			
		</div>
	</div>
</div>

<?php else: ?>
<?php $categories = get_the_category(); ?>
<div class="city-blog-image-fp">
	<div class="blog-info-wrap-grid-fp <?php foreach($categories as $category) { echo $category->slug . '  '; }; ?>">
		<a class="blog-image-single-fp" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'blog-grid', array( 'class' => 'img-responsive' ) ); ?></a>
		<div class="city-blog-content-fp">
			<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<hr>
			<hr>
			
			<div class="text">
				<?php $content = get_field('content'); ?>
				<p><?php echo wp_trim_words($content, 25, ' ...' ) ; ?></p>
			</div>
			<div class="city-blog-extras-fp">
				<p><?php echo get_the_date(); ?></p>
				
			</div>
			
		</div>
	</div>
</div>
<?php endif; ?>
<script>
jQuery(document).ready(function($) {
$grid = $('.blog-grid').isotope({
itemSelector: '.blog-info-wrap-grid-fp',
layoutMode: 'packery',
packery: {
gutter: 20
}
});
//Allows images to load before isotope grid loads
$grid.imagesLoaded().progress( function() {
$grid.isotope('layout');
});
$('.filter-wrap').on('click', 'a', function() {
event.preventDefault();
var filterValue = $(this).attr('data-filter');
$grid.isotope({
filter: filterValue
});
});
});
</script>