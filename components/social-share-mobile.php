<div class="row">
	<div class="col-md-12">
		<div class="social-share-wrapper-mobile">
			<a href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php the_field('pinterest_image'); ?>&description=<?php the_title(); ?>" target="_blank"><i class="fab fa-pinterest-square pinterest"></i></a>
			<a href="https://www.facebook.com/sharer/sharer.php?u=www.mapscratchers.com/%3C?php%20the_permalink();%20?%3E" target="_blank"><i class="fab fa-facebook-square facebook"></i></a>
			<a href="https://twitter.com/share?url=<?php echo the_permalink(); ?>" target="_blank">
				<i class="fab fa-twitter-square twitter" ></i>
			</a>
			
		</div>
	</div>
</div>