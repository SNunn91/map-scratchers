
<?php /* 

OLD CODE

- This code was used to pull city names and create carousels with blogs in them.
- Now the country page is using isotope filters


<!-- Portugal -->
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php if($cityTitle =='Lisbon'): ?>
	<?php
	$args = array( 'posts_per_page' => -1,
	'post_type' => 'post',
	'category_name' => 'lisbon',
	'orderby'   => 'menu_order',
	'order'    => 'DESC'
	);
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "lisbon")); ?>
	<?php get_template_part( 'components/city-blog-info'); ?>
	<?php endforeach;
	wp_reset_postdata(); ?>
</div>


<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Sintra'): ?>
	<?php
	$args = array( 'posts_per_page' => -1,
	'post_type' => 'post',
	'category_name' => 'sintra',
	'orderby'   => 'menu_order',
	'order'    => 'DESC'
	);
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "sintra")); ?>
	
	<?php get_template_part( 'components/city-blog-info'); ?>
	<?php endforeach;
	wp_reset_postdata(); ?>
</div>
<!-- Portugal end -->


<!-- Italy -->
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Rome'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'rome',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "rome")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Venice'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'venice',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "venice")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Bologna'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'bologna',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "bologna")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Verona'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'verona',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "verona")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Florence'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'florence',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "florence")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Pisa'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'pisa',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "pisa")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<!-- Italy end-->

<!-- Russia -->
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Moscow'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'moscow',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "moscow")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Saint Petersburg'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'saint-petersburg',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "saint-petersburg")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<!-- Russia end -->

<!-- China -->
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Chengdu'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'chengdu',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "chengdu")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Beijing'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'beijing',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "beijing")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Xi\'an'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'xian',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "xian")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<!-- China end -->

<!-- USA -->
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'San Francisco'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'san-francisco',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "san-francisco")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>

<!-- Canada -->
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Toronto'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'toronto',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "toronto")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>
<div class="blog-carousel">
	<?php $cityTitle = get_sub_field('city_title'); ?>
	<?php elseif($cityTitle == 'Niagara Falls'): ?>
		<?php
		$args = array( 'posts_per_page' => -1,
		'post_type' => 'post',
		'category_name' => 'niagara-falls',
		'orderby'   => 'menu_order',
		'order'    => 'DESC'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<?php $category_city = wp_get_post_terms($post->ID, 'category', array("fields" => "niagara-falls")); ?>
		<?php get_template_part( 'components/city-blog-info'); ?>
		<?php endforeach;
		wp_reset_postdata(); ?>
</div>


<?php endif; ?>

*/ ?>
