<?php get_template_part( 'head' ); ?>
<header id="header" class="navigation">
	<!-- Desktop pre-nav -->
	
	<!-- Desktop nav -->
	<section class="hidden-xs hidden-sm">
		<div class="social-nav-wrapper">
			<div class="container">
				<div class="row">
					<div class="social-media">
					<div class="icons">
						<a href="https://www.pinterest.co.uk/mapscratchers/" target="_blank"><i class="fab fa-pinterest-square"></i></a>	
						<a href="https://www.youtube.com/channel/UCw_vNR2ovhhOVMdhSxE8t3A" target="_blank"><i class="fab fa-youtube"></i></a>
						<a href="https://www.instagram.com/mapscratchers/" target="_blank"><i class="fab fa-instagram"></i></a>
						<a href="https://www.bloglovin.com/@anispee" target="_blank"><i class="fa fa-heart"></i></a>
						<a href="" target="_blank"><i class="fab fa-facebook-square"></i></a>
						
					</div>
				</div>
				</div>
			</div>
			
		</div>
		<div class="container">
			<div class="row">
				<div class="primary-nav">
					<div class="nav-heading">
						<h3 class="nav-heading"><a href="<?php echo get_permalink(get_page_by_title('Home')); ?>">Map Scratchers</a></h3>
					</div>
					
					
					<div class="primary-menu">
						<?php
								wp_nav_menu( array(
								'menu'  => 'Main Menu'
								));
							?>
					</div>
					
				</div>
				
			</div>
		</div>
	</section>
	<!-- Mobile nav -->
	<section class="hidden-md hidden-lg">
		<div id="myNav" class="overlay">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			<div class="overlay-content">
				<div class="social-media">
					<div class="icons">
						<a href="https://www.pinterest.co.uk/mapscratchers/" target="_blank"><i class="fab fa-pinterest-square"></i></a>	
						<a href="https://www.youtube.com/channel/UCw_vNR2ovhhOVMdhSxE8t3A" target="_blank"><i class="fab fa-youtube"></i></a>
						<a href="https://www.instagram.com/mapscratchers/" target="_blank"><i class="fab fa-instagram"></i></a>
						<a href="https://www.bloglovin.com/@anispee" target="_blank"><i class="fa fa-heart"></i>	</a>
						<a href="https://www.facebook.com/mapscratchers/" target="_blank"><i class="fab fa-facebook-square"></i></a>
					</div>
				</div>
				<?php
					wp_nav_menu( array(
					'menu'  => 'Main Menu'
					));
				?>
			</div>
		</div>
		<div class="mobile-menu-wrap">
			<span class="menu-burger" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
		<h3 class="nav-heading"><a href="<?php echo get_permalink(get_page_by_title('Home')); ?>">Map Scratchers</a></h3>
		</div>
		
	</section>
	
</header>



<script>
jQuery(window).ready(function($) {
        //headroom 
		var myElement = document.querySelector(".navigation");
      	var headroom = new Headroom(myElement);
      	headroom.init();
       
    });
	
</script>
<script>
function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}
</script>

