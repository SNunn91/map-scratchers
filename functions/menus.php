<?php
// ***********************************************************************//
// Menus
// ***********************************************************************//
// Add Menus

function register_my_menus() {
    register_nav_menus(array(
        'primary_menu' => 'Main Menu',
        'footer_menu' => 'Footer Menu'
        ));
}
add_action('init', 'register_my_menus');

