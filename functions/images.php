<?php
// ***********************************************************************//
// Images
// ***********************************************************************//

// Images: Add Featured Images Support
add_theme_support('post-thumbnails');

// Images: Add custom featured image sizes
if (function_exists('add_image_size')) {
    add_image_size('square', 250, 250, false);
    add_image_size('blog-grid', 706, 9999, false);
}

// Images: Get SVG content 
// Example: get_svg('images/example.svg')
function get_svg($image){
	if (file_exists(TEMPLATEPATH . '/' . $image)) {
	echo file_get_contents(TEMPLATEPATH . '/' . $image);
	} else {
		echo 'Could not find ' . TEMPLATEPATH . '/' . $image;
	}
}