<?php
// ***********************************************************************//
// Scripts
// ***********************************************************************//

// Add function to enqueue extra scripts
add_action('wp_enqueue_scripts', 'cubiq_load_javascript_files');

// Register the scripts
function cubiq_load_javascript_files() {
    // Don't bother adding scripts here anymore. Compile them in CodeKit by selecting plugins.js and change imports there.
	//wp_register_script('odometer', get_template_directory_uri() . '/js/plugins/odometer.min.js', array('jquery'), true);
    // wp_register_script('theme', get_template_directory_uri() . '/js/theme-min.js', array('jquery'), true);
    //general
    wp_register_script('headroom', get_template_directory_uri() . '/js/plugins/min/headroom-min.js', array('jquery'), true);
    wp_register_script('flickity', get_template_directory_uri() . '/js/plugins/flickity.pkgd.min.js', array('jquery'), true);
    wp_register_script('isotope', get_template_directory_uri() . '/js/plugins/isotope.pkgd.min.js', array('jquery'), true);
    wp_register_script('packery', get_template_directory_uri() . '/js/plugins/packery.pkgd.min.js', array('jquery'), true);
    wp_register_script('insta', get_template_directory_uri() . '/js/instagram/instagram_feed.js', array('jquery'), true);
    wp_register_script('gsap', get_template_directory_uri() . '/js/TweenMax.min.js', array('jquery'), true);
    //Jump back to top JS
    wp_register_script('back-to-top', get_template_directory_uri() . '/js/snippets/back-to-top.js', array('jquery'), true);
    // //Fade in JS
    // wp_register_script('fade-in', get_template_directory_uri() . '/js/snippets/fade-in.js', array('jquery'), true);
    //AOS JS
    wp_register_script('aos', get_template_directory_uri() . '/js/snippets/aos.js', array('jquery'), true);
    



    //Maps
    wp_register_script('ammap', get_template_directory_uri() . '/js/map/ammap.js', array('jquery'), true);
    wp_register_script('ammapExtension', get_template_directory_uri() . '/js/map/ammap_amcharts_extension.js', array('jquery'), true);
    wp_register_script('worldLow', get_template_directory_uri() . '/js/map/worldLow.js', array('jquery'), true);
    wp_register_script('worldHigh', get_template_directory_uri() . '/js/map/worldHigh.js', array('jquery'), true);
    wp_register_script('light', get_template_directory_uri() . '/js/map/light.js', array('jquery'), true);

    //Individual Country Maps
    // wp_register_script('portugalRegionsLow', get_template_directory_uri() . '/js/map/countries/portugalRegionsLow.js', array('jquery'), true);
    // wp_register_script('germanyLow', get_template_directory_uri() . '/js/map/countries/germanyLow.js', array('jquery'), true);
    // wp_register_script('italyLow', get_template_directory_uri() . '/js/map/countries/italyLow.js', array('jquery'), true);
   


   
    // Get the scripts ready for use
   // wp_enqueue_script('odometer');
    wp_enqueue_script('isotope');
    wp_enqueue_script('packery');
    wp_enqueue_script('headroom');
    wp_enqueue_script('flickity');
    wp_enqueue_script('insta');
    wp_enqueue_script('gsap');
    wp_enqueue_script('back-to-top');
    //maps
    wp_enqueue_script('ammap');
    wp_enqueue_script('ammapExtension');
    wp_enqueue_script('worldLow');
     wp_enqueue_script('worldHigh');
    wp_enqueue_script('light');
    // //fade in
    // wp_enqueue_script('fade-in');
    //AOS
    wp_enqueue_script('aos');

    // //individual maps
    // wp_enqueue_script('portugalRegionsLow');
    // wp_enqueue_script('germanyLow');
    // wp_enqueue_script('italyLow');

}