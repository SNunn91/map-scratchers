<?php
// ***********************************************************************//
// Base Theme Functions
// ***********************************************************************//
require_once (TEMPLATEPATH . '/functions/admin.php');
require_once (TEMPLATEPATH . '/functions/scripts.php');
require_once (TEMPLATEPATH . '/functions/styles.php');
require_once (TEMPLATEPATH . '/functions/images.php');
require_once (TEMPLATEPATH . '/functions/menus.php');
require_once (TEMPLATEPATH . '/functions/comments.php');

// ***********************************************************************//
// Optional
// ***********************************************************************//
// require_once (TEMPLATEPATH . '/functions/dev.php');
// require_once (TEMPLATEPATH . '/functions/image-filters.php');
// require_once (TEMPLATEPATH . '/functions/excerpts.php');

add_filter('jpeg_quality', function($arg){return 100;});


//remove white space and special characters
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
