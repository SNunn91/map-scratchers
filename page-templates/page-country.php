<?php /*Template Name: Country Template*/ get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<?php  get_template_part( 'components/title'); ?>
<?php $country = get_the_title(); //Pull the country page title ?>
<?php if($country == 'Italy') :
$subcat = array('child_of' => 13); ?>
<?php elseif($country == 'Portugal') :
$subcat = array('child_of' => 10); ?>
<?php elseif($country == 'Russia') :
$subcat = array('child_of' => 24); ?>
<?php elseif($country == 'USA') :
$subcat = array('child_of' => 47); ?>
<?php elseif($country == 'Canada') :
$subcat = array('child_of' => 43); ?>
<?php elseif($country == 'China') :
$subcat = array('child_of' => 40); ?>
<?php elseif($country == 'Germany') :
$subcat = array('child_of' => 19); ?>
<?php elseif($country == 'Croatia') :
$subcat = array('child_of' => 48); ?>
<?php endif; ?>

	<?php $categories = get_categories( $subcat ); ?>
	<section class="blog-main-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="filter-wrap">
						
						
						<?php if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
						foreach ( $categories as $category ) {
						echo '<li><a class="animated-button filter-button" href="#/" data-filter=".' . $category->slug . '">' . $category->name . '</a></li>';
						
						}
						} ?>
						<li><a class="animated-button filter-button" data-filter="*" href="#">Show All</a></li>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="blog-grid">
						
						
						<?php
						$args = array( 'posts_per_page' => -1,
						'post_type' => 'post',
						'category' => $subcat,
						'orderby'   => 'menu_order',
						'order'    => 'DESC'
						);
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						
						<?php get_template_part( 'components/city-blog-info-grid-item'); ?>
						<?php endforeach;
						wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php /*
	OLD CODE:
	This code pulls through the country map and the a list of cities
	<section class="introduction">
				<div class="container">
							<div class="row">
										<div class="col-sm-12">
													<div class="main-intro">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="city-wrap">
						<h2><?php the_field('section_heading') ?></h2>
						
						<ul>
							<?php
								// check if the repeater field has rows of data
								if( have_rows('cities_state_repeater') ):
									// loop through the rows of data
							while ( have_rows('cities_state_repeater') ) : the_row(); ?>
							<?php $test = str_replace(' ', '', get_sub_field('city_state'));
									$x = preg_replace('/[^A-Za-z0-9\-]/', '', $test);
										
							?>
							<div class="cities">
								<i class="fa fa-globe-africa"></i>
								<li class="city-list">
									<a href="#<?php echo $x; ?>">
										<h4><?php the_sub_field('city_state');  ?></h4>
									</a>
								</li>
							</div>
							<?php endwhile;
							else :
							// no rows found
							endif;
							?>
							
						</ul>
						
					</div>
					
				</div>
				<div class="col-md-8">
					<?php  get_template_part( 'components/maps'); ?>
				</div>
				
			</div>
		</div>
		
	</section>
	*/ ?>
	<?php /*
	OLD CODE:
	This code was used ti pull the city titles and populate them with their respective blogs.
	<?php  if( get_row_layout() == 'city_section' ): ?>
	<section class="city">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php $test = str_replace(' ', '', get_sub_field('city_title'));
							$x = preg_replace('/[^A-Za-z0-9\-]/', '', $test);
							
					?>
					
					<h2 id="<?php echo $x; ?>"><?php the_sub_field('city_title'); ?></h2>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="city-content">
						<?php the_sub_field('city_content'); ?>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<?php get_template_part( 'components/city-blog'); ?>
					
					
					
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<?php   endwhile;
	else :
	// no layouts found
	endif;
	?>
	*/ ?>
	<?php get_template_part('components/back-to-top') ?>
	<hr>
	<hr>
	<?php get_template_part( 'components/all-blogs'); ?>
	<?php endwhile; ?>
	<script>
	jQuery(document).ready(function($) {
		$('.blog-carousel').flickity({
			// options
			cellAlign: 'left',
			groupCells:3,
			contain: false,
			pageDots: false,
			wrapAround: true,
			prevNextButtons: true
		});
		//navigation
		// $('.previous-small-team').on( 'click', function() {
				// 	$('.meet-the-team-carousel').flickity( 'previous');
		// });
		// $('.next-small-team').on( 'click', function() {
				// 	$('.meet-the-team-carousel').flickity( 'next');
		// });
	});
	</script>
	<script>
	jQuery(document).ready(function($) {
			//smooth scroll
		$(document).on('click', '.city-list a', function(event){
	event.preventDefault();
	$('html, body').animate({
	scrollTop: $( $.attr(this, 'href') ).offset().top
	}, 500);
	});
	});
	</script>
	<?php get_footer(); ?>