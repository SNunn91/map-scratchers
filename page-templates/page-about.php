<?php /*Template Name: About Template */ get_header(); ?>
<section class="about-section">
	<div class="container">
		<div class="about-section-one-wrapper">
			<div class="row">
				<div class="col-md-8">
					<div class="about-section-one-text">
						<h2>Hello There,</h2>
						<p><?php the_field('about_text_one'); ?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="about-section-one-image">
						<?php if( get_field('about_image_one') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_one'); ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			
		</div>
		<div class="about-section-two-wrapper">
			<div class="row">
				<div class="col-md-12">
					<div class="about-section-two-text">
						<p><?php the_field('about_text_two'); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-section-three-wrapper">
			<div class="row">
				<div class="col-md-6">
					<div class="about-section-three-image-one">
						<?php if( get_field('about_image_two') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_two'); ?>" />
						<?php endif; ?>
					</div>
					
				</div>
				<div class="col-md-6">
					<div class="about-section-three-image-two">
						<?php if( get_field('about_image_three') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_three'); ?>" />
						<?php endif; ?>
					</div>
					
				</div>
			</div>
		</div>
		<div class="about-section-four-wrapper">
			<div class="row">
				<div class="col-md-4">
					<div class="about-section-four-image">
						<?php if( get_field('about_image_four') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_four'); ?>" />
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-8">
					<div class="about-section-four-text">
						<p><?php the_field('about_text_three'); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-section-five-wrapper">
			<div class="row">
				<div class="col-md-12">
					<div class="about-section-five-text-one">
						<p><?php the_field('about_text_four'); ?></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="about-section-five-image-one">
						<?php if( get_field('about_image_five') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_five'); ?>" />
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="about-section-five-image-two">
						<?php if( get_field('about_image_six') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_six'); ?>" />
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="about-section-five-image-three">
						<?php if( get_field('about_image_seven') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_seven'); ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="about-section-five-text-two">
						<p><?php the_field('about_text_five'); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-section-six-wrapper">
			<div class="row">
				<div class="col-md-12">
					<div class="about-section-six-image">
						<?php if( get_field('about_image_eight') ): ?>
						<img class="img-responsive center-block" src="<?php the_field('about_image_eight'); ?>" />
						<?php endif; ?>
					</div>
					<div class="hidden-xs hidden-sm about-section-six-text">
						<p><?php the_field('about_text_six'); ?></p>
					</div>
					<div class="hidden-md hidden-lg about-section-six-text-alt">
						<p><?php the_field('about_text_six'); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-section-seven-wrapper">
			<div class="row">
				<div class="col-md-12">
					<div class="about-section-seven-text">
						<p><?php the_field('about_text_seven'); ?></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="about-section-seven-image">
						<?php if( get_field('about_image_nine') ): ?>
						<img class="img-responsive center-block" src="<?php the_field('about_image_nine'); ?>" />
						<?php endif; ?>
						<div class="hidden-xs hidden-sm text"><h2>Bandit</h2></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="about-section-seven-image">
						<?php if( get_field('about_image_ten') ): ?>
						<img class="img-responsive center-block" src="<?php the_field('about_image_ten'); ?>" />
						<?php endif; ?>
						<div class="hidden-xs hidden-sm text"><h2>Flacco</h2></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="about-section-seven-text">
						<p><?php the_field('about_text_eight'); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-section-eight-wrapper">
			<div class="row">
				<div class="col-md-8">
					<div class="about-section-eight-text">
						<p><?php the_field('about_text_nine'); ?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="about-section-eight-image">
						<?php if( get_field('about_image_eleven') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_eleven'); ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<?php get_template_part('components/back-to-top') ?>
<div id="disqus_thread"></div>
<script>
/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://www-mapscratchers-com.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<?php get_footer(); ?>