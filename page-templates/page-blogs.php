<?php /*Template Name: Blog Template*/ get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part( 'components/title'); ?>
<section class="blog-main-section blog-main-section--extdended">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="filter-wrap">
					<?php $tags = get_tags();
					if ( ! empty( $tags ) && ! is_wp_error( $tags ) ){
					foreach ( $tags as $tag ) {
					echo '<li><a class="btn btn-sm animated-button filter-button" href="#/" data-filter=".' . $tag->slug . '">' . $tag->name . '</a></li>';
					}
					} ?>
					<li><a class="animated-button filter-button" data-filter="*" href="#">Show All</a></li>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="blog-grid">
					
					
					<?php
					$args = array( 'posts_per_page' => -1,
					'post_type' => 'post',
					'orderby'   => 'menu_order',
					'order'    => 'DESC'
					);
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					
					<?php get_template_part( 'components/city-blog-info-grid-item'); ?>
					<?php endforeach;
					wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_template_part('components/back-to-top') ?>
<script>
jQuery(document).ready(function($) {
	$('.blog-carousel').flickity({
		// options
		cellAlign: 'left',
		groupCells:3,
		contain: false,
		pageDots: false,
		wrapAround: true,
		prevNextButtons: true
	});
	//navigation
	// $('.previous-small-team').on( 'click', function() {
		// 	$('.meet-the-team-carousel').flickity( 'previous');
	// });
	// $('.next-small-team').on( 'click', function() {
		// 	$('.meet-the-team-carousel').flickity( 'next');
	// });
});
</script>

<?php endwhile; ?>
<?php get_footer(); ?>