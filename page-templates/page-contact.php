<?php /* Template Name: Contact Template */ get_header(); ?>
<?php  get_template_part( 'components/title'); ?>
<section class="contact-form-section">
	<div class="container">
		<div class="contact-form-wrapper">
			<div class="row">
				<div class="col-md-6">
					<div class="contact-text">
						<h2>Let Us Know!</h2>
						<p><?php the_field('contact_us_text') ?></p>
					</div>
					<div class="contact-image">
						<?php if( get_field('contact_us_image') ): ?>
						<img class="img-responsive center-block" src="<?php the_field('contact_us_image'); ?>" />
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-6">
					<?php echo do_shortcode('[contact-form-7 id="18" title="Contact form"]') ?>
				</div>	
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>