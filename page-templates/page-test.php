<?php /*Template Name: test Template */ get_header(); ?>
<?php  get_template_part( 'components/title'); ?>
<section class="about-us">
	<div class="container">
		<div class="row">
			<!-- First Row -->
			<div class="about-section-one-wrapper">
				<div class="about-section-one-text">
					<h2>Hello There,</h2>
					<p><?php the_field('about_text_one'); ?></p>
				</div>
				<div class="about-section-one-image">
					<?php if( get_field('about_image_one') ): ?>
					<img class="img-responsive" src="<?php the_field('about_image_one'); ?>" />
					<?php endif; ?>
				</div>
			</div>
			<!-- Second Row -->
			<div class="about-section-two-wrapper">
				<div class="about-section-two-text">
					<p><?php the_field('about_text_two'); ?></p>
				</div>
			</div>
			<!-- Section Three -->
			<div class="about-section-three-wrapper">
				<div class="col-md-6">
					<div class="about-section-three-image one">
						<?php if( get_field('about_image_two') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_two'); ?>" />
						<?php endif; ?>
					</div>
					
				</div>
				<div class="col-md-6">
					<div class="about-section-three-image two">
						<?php if( get_field('about_image_three') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_three'); ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- Section Four -->
			<div class="about-section-four-wrapper">
				<div class="about-section-four-image">
					<?php if( get_field('about_image_four') ): ?>
					<img class="img-responsive" src="<?php the_field('about_image_four'); ?>" />
					<?php endif; ?>
				</div>
				<div class="about-section-four-text">
					<p><?php the_field('about_text_three'); ?></p>
				</div>
			</div>
			<!-- Section Five -->
			<div class="about-section-five-wrapper">
				<div class="col-md-12">
					<div class="about-five-text-one">
						<p><?php the_field('about_text_four'); ?></p>
					</div>
				</div>
				<div class="about-five-image-wrap">
					<div class="about-five-image about-five-image-one">
						<?php if( get_field('about_image_five') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_five'); ?>" />
						<?php endif; ?>
					</div>
					<div class="about-five-image about-five-image-two">
						<?php if( get_field('about_image_six') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_six'); ?>" />
						<?php endif; ?>
					</div>
					<div class="about-five-image about-five-image-three">
						<?php if( get_field('about_image_seven') ): ?>
						<img class="img-responsive" src="<?php the_field('about_image_seven'); ?>" />
						<?php endif; ?>
					</div>
				</div>
				
				<div class="hidden-xs hidden-sm about-five-text-two">
					<p><?php the_field('about_text_five'); ?></p>
				</div>
				<div class="hidden-md hidden-lg">
					<div class="col-xs-12 cold-sm-12">
						<div class="about-five-text-two">
							<p><?php the_field('about_text_five'); ?></p>
						</div>
					</div>
				</div>
				
			</div>
			<!-- Section Six -->
			<div class="about-section-six-wrapper">
				<div class="about-section-six-image">
					<?php if( get_field('about_image_eight') ): ?>
					<img class="img-responsive center-block" src="<?php the_field('about_image_eight'); ?>" />
					<?php endif; ?>
				</div>
				<div class="hidden-xs hidden-sm about-section-six-text">
					<p><?php the_field('about_text_six'); ?></p>
				</div>
				<div class="hidden-md hidden-lg about-section-six-text-alt">
					<p><?php the_field('about_text_six'); ?></p>
				</div>
			</div>
			<!-- Section seven -->
			<div class="about-section-seven-wrapper">
				<div class="col-md-12">
					<div class="about-section-seven-text">
						<p><?php the_field('about_text_seven'); ?></p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-md-6">
					<div class="about-section-seven-image">
						<?php if( get_field('about_image_nine') ): ?>
						<img class="img-responsive center-block" src="<?php the_field('about_image_nine'); ?>" />
						<?php endif; ?>
						<div class="hidden-xs hidden-sm text"><h2>Bandit</h2></div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-md-6">
					<div class="about-section-seven-image">
						<?php if( get_field('about_image_ten') ): ?>
						<img class="img-responsive center-block" src="<?php the_field('about_image_ten'); ?>" />
						<?php endif; ?>
						<div class="hidden-xs hidden-sm text"><h2>Flacco</h2></div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="about-section-seven-text">
						<p><?php the_field('about_text_eight'); ?></p>
					</div>
				</div>
			</div>
			<!-- Section nine -->
			<div class="about-section-eight-wrapper">
				<div class="hidden-xs hidden-sm hidden-md about-section-eight-text">
					
					<p><?php the_field('about_text_nine'); ?></p>
				</div>
				<div class="about-section-eight-image">
					<?php if( get_field('about_image_eleven') ): ?>
					<img class="img-responsive" src="<?php the_field('about_image_eleven'); ?>" />
					<?php endif; ?>
				</div>
				<div class="hidden-lg about-section-eight-text">
					
					<p><?php the_field('about_text_nine'); ?></p>
				</div>
			</div>
			
		</div>
	</div>
</section>
<?php get_template_part('components/back-to-top') ?>
<?php get_footer(); ?>