<?php /*Template Name: Germany Template*/ get_header(); ?>

<?php while (have_posts()) : the_post(); ?>


	<section class="main-image " style="background-image:url('<?php the_field('main_image'); ?>');">
		<h1><?php the_title(); ?></h1>
		
	</section>
	<section class="introduction">
		<?php the_content(); ?>
	</section>

<?php endwhile; ?>

<?php get_footer(); ?>