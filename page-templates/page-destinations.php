<?php /*Template Name: Destinations Template */ get_header(); ?>
<?php  get_template_part( 'components/title'); ?>
<section class="destinations">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="destination-wrap">
					<p>CLICK TO EXPAND</p>
					<div class="destination-accordion" id="destination-accordion">
						<?php
							$counter = 1;
							// check if the repeater field has rows of data
							if( have_rows('continent_repeater') ):
								// loop through the rows of data
						while ( have_rows('continent_repeater') ) : the_row(); ?>
						<div class="destination-block">

							<div class="destination-header">
								<i class="fa fa-globe-africa"></i>
								<a class="card-link" data-toggle="collapse" href="#<?php echo $counter; ?>">
									<h4><?php the_sub_field('continent'); ?></h4>
								</a>	
							</div>
							<div id="<?php echo $counter; ?>" class="collapse" data-parent="#accordion">
								<div class="countries">
									<ul>
									<?php if( have_rows('country_repeater') ):
									// loop through the rows of data
									while ( have_rows('country_repeater') ) : the_row(); ?>
									
										<li class="individual-country"><a href="/<?php the_sub_field('country'); ?>">
										<h5><?php the_sub_field('country'); ?></h5></a></li>
									
									<?php
									endwhile;
									else :
									// no rows found
									endif;
									
									?>
									</ul>
								</div>
							</div>
						</div>
						<?php $counter++;
						endwhile;
							else :
							// no rows found
								endif;
						?>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<?php  get_template_part( 'components/world-map'); ?>
			</div>
		</div>
		
	</div>
</section>
<?php get_template_part('components/back-to-top') ?>
<?php get_footer(); ?>