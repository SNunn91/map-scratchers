<?php /*Template Name: Map test Template*/ get_header(); ?>


<div id="mapdiv" ></div>

<script>
// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
var currentObject;
var map = AmCharts.makeChart( "mapdiv", {
  "type": "map",
  "theme": "light",

  "imagesSettings": {
    "rollOverColor": "#089282",
    // "rollOverScale": 3,
    // "selectedScale": 3,
    "selectedColor": "#089282",
    "color": "#13564e"
  },

  "zoomControl": {
    "buttonFillColor": "#efd2c1"
  },

  "areasSettings": {
  	"color" : "#efd2c1",
	"selectedColor" : "#6c648b",
	"rollOverColor" : "#bfa99b",
	"rollOverOutlineColor": "#6c648b",
    "autoZoom": false,
    
  },

  "dataProvider": {
    "map": "worldLow",
    "getAreasFromMap": true,
		    "areas" :
		[
			{
				"id": "DE",
				"showAsSelected": true,
				"myUrl": "https://www.amcharts.com/",
				"selectable" : true

			},
			{
				"id": "IT",
				"showAsSelected": true
			},
			{
				"id": "PT",
				"showAsSelected": true
			}
		]
		},
    // "images": [ {

    //   "id": "Portugal",
    //   "color": "#6c648b",
    //   "svgPath": targetSVG,
    //   "zoomLevel": 5,
    //   "scale": 2,
    //   "title": "Portugal",
    //   "latitude": 	38.736946,
    //   "longitude": -9.142685,
    //   "myUrl": "https://www.amcharts.com/"
    // } ]
  // },

  "listeners": [ {
    "event": "clickMapObject",
    "method": function( event ) {
      // check if the map is already at traget zoomLevel and go to url if it is
    
        window.location.href = event.mapObject.myUrl;
    
      currentObject = event.mapObject;
    }
  }]
} );

function clickObject( id ) {
  map.clickMapObject( map.getObjectById( id ) );
}
</script>




<?php get_footer(); ?>