<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part( 'components/title'); ?>
<section class="blog-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="extra">
					<h4> Posted - <?php the_date(); ?></h4>
					<h4></h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="content-wrap">
					<?php the_field('content'); ?>
				</div>
			</div>
		</div>
		<div class="hidden-md hidden-lg">
			<?php get_template_part('components/social-share-mobile') ?>
		</div>
		
	</div>
	<hr>
	<hr>
	<div class="save-pinterest">
		<div class="row">
			<div class="col-md-12">
				<h2>Pin for later...</h2>
				<div class="pinterest-image">
					<?php if( get_field('pinterest_image') ): ?>
					<img class="img-responsive" src="<?php the_field('pinterest_image'); ?>" />
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="hidden-xs hidden-sm">
	<?php get_template_part('components/social-share') ?>
</div>

<?php get_template_part('components/back-to-top') ?>
<?php endwhile; ?>
<hr>
<hr>
<div id="disqus_thread"></div>
<script>
/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://www-mapscratchers-com.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<?php get_footer(); ?>